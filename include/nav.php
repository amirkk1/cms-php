 <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluled">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">זהבה גנים</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="index.php">מסד נתונים</a></li>
              <li><a href="#">(יומן פגישות (בקרוב</a></li>
              <li><a href="feedback.php">פידבק לאמיר</a></li>
              <form class="navbar-form navbar-left" action="search.php" method="post"  role="search">
        <div class="input-group">
                            <input name="search" type="text" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn btn-default" name="submit" type="submit">
                                    <span class="glyphicon glyphicon-search"></span>
                            </button>
                            </span>
                        </div>
      </form>
      
            </ul>
            <ul class="nav navbar-nav navbar-right">
               <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>התחבר</b> <span class="caret"></span></a>
            <ul id="login-dp" class="dropdown-menu">
                <li>
                     <div class="row">
                            <div class="col-md-12">
                               
                                
                                 <form class="form" role="form" method="post" action="admin/login_area/login.php" accept-charset="UTF-8" class="text-left" id="login-nav">
                                        <div class="form-group">
                                             <label class="sr-only" for="exampleInputEmail2">משתמש</label>
                                             <input name="username" type="text" class="form-control text-left" id="exampleInputEmail2" placeholder="שם משתמש" required>
                                        </div>
                                        <div class="form-group">
                                             <label  class="sr-only" for="exampleInputPassword2">סיסמה</label>
                                             <input name="password" type="password" class="form-control text-left" id="exampleInputPassword2" placeholder="סיסמה" required>
                                             <div class="help-block text-left"><a href="">שכחת סיסמה?</a></div>
                                        </div>
                                        <div class="form-group">
                                             <button type="submit" name="submit" class="btn btn-primary btn-block">התחבר</button>
                                        </div>
                                        <div class="checkbox">
                                             <label>
                                             <input type="checkbox" class="remmberme"> ? זכור אותי
                                             </label>
                                        </div>
                                 </form>
                            </div>
                            
                     </div>
                </li>
            </ul>
        </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>