
        <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p>&copy;<a href="https://il.linkedin.com/in/amir-cohen-ba35b9100" target="_blank" >  <i class="fa fa-linkedin-square fa-3x"></i></a> </p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
    </footer>

        </div>
    <!-- /.container -->
 <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>