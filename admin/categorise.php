<?php include "include/admin_header.php" ?>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include "include/admin_nav.php" ?>


        <div id="page-wrapper">



            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            wellcome to admin page
                            <small>Subheading</small>
                        </h1>

                                <div class="col-md-6">

                                             <?php 
                                             //add new category
                                                if(isset($_POST['submit'])) {
                                                    $cat_title = $_POST['cat_title'];

                                                    if($cat_title == "" || empty($cat_title)){
                                                        echo "this should not be empty";
                                                    }else{
                                                    $query = "INSERT INTO categorise(cat_title)";
                                                    $query .="VALUE ('{$cat_title}')";
                                                    $add_cat = mysqli_query($connect,$query);
                                                    if(!$add_cat){
                                                        die("fail to add cat" . mysqli_error($connect));
                                                    }
                                                }

                                            }
                                             ?>

                                    <form action="" method="post">
                                            <div class="form-group">
                                                <input type="text" name="cat_title" class="form-control" placeholder="category title">
                                            </div>
                                             <div class="form-group">
                                                <input type="submit" name="submit" class="btn btn-primary" value="add Category">
                                            </div>
                                    </form>

                                    <form action="" method="post">
                                            <div class="form-group">

                                                 <?php 
                                             //add edit  category
                                                 if(isset($_GET['edit'])){
                                                    $cat_id = $_GET['edit'];
                                                    $query = "SELECT * FROM  categorise WHERE cat_id = $cat_id ";
                                                    $selcet_cat = mysqli_query($connect,$query);

                                                        while($row = mysqli_fetch_assoc($selcet_cat)){
                                                            $cat_id =  $row['cat_id'];
                                                            $cat_title =  $row['cat_title'];

                                                            ?>

                                                             <input type="text" value="<?php if(isset($cat_title)){echo $cat_title;} ?>" name="cat_title" class="form-control" >
                                          <?php  }}?>



                                          <?php 

                                              if(isset($_POST['update_categorise'])) {
                                                    $edit_cat = $_POST['update_categorise'];

                                                    $query = "UPDATE FROM categorise WHERE cat_title = '{$edit_cat}' WHERE cat_id = {$cat_id}";
                                                    $update_query = mysqli_query($connect,$query);
                                                    if(!$update_query){
                                                        die("fail to update" . mysqli_error($connect));
                                                    }
                                                }


                                          ?>

                                            </div>
                                             <div class="form-group">
                                                <input type="submit" name="submit" class="btn btn-primary" value="Update">
                                            </div>
                                    </form>
                                </div>

                                <div class="col-md-6">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>id</td>
                                                    <th>category title</td>
                                                    <th>delete category</td>
                                                    <th>edit category</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                   
                                                      <?php 
                                                      //find all category
                                                    $query = "SELECT * FROM  categorise";
                                                    $selcet_cat = mysqli_query($connect,$query);

                                                        while($row = mysqli_fetch_assoc($selcet_cat)){
                                                            $cat_id =  $row['cat_id'];
                                                            $cat_title =  $row['cat_title'];
                                                            echo "<tr>";
                                                            echo "<td>{$cat_id}</td>";
                                                            echo "<td>{$cat_title}</td>";
                                                            echo "<td><a class='btn btn-primary' href='categorise.php?delete={$cat_id}'>delete</td>";
                                                            echo "<td><a class='btn btn-primary' href='categorise.php?edit={$cat_id}'>edit</td>";
                                                            echo "</tr>";
                                                        }
                                                     ?>   


                                                           <?php 
                                             //delete category
                                                if(isset($_GET['delete'])) {
                                                    $delete_cat = $_GET['delete'];

                                                    $query = "DELETE FROM categorise WHERE cat_id = {$delete_cat}";
                                                    $delete_query = mysqli_query($connect,$query);
                                                    Header("Location: categorise.php");

                                            }
                                             ?>
                                                
                                            <tbody>
                                        </table>    
                                </div>
                                    
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    

    <?php include "include/admin_footer.php" ?>