<div id="menu2" class="tab-pane fade">
                      <h3>הוסף  גן </h3>
                        <form action="" method="post" class="form-inline">
                            <div class="form-group">
                              <label for="gan_name">שם הגן:</label>
                              <input type="text" class="form-control" name="gan_name">
                            </div>
                            <div class="form-group">
                              <label for="gan_number">מספר הגן:</label>
                              <input type="tel" class="form-control" name="gan_number">
                            </div>
                            <div class="form-group">
                              <label for="address">כתובת:</label>
                              <input type="text" class="form-control" name="address">
                            </div>
                            <div class="form-group">
                              <label for="owner">שם הבעלים:</label>
                              <input type="text" class="form-control" name="owner">
                            </div>
                            <div class="form-group">
                              <label for="owner_number">טלפון הבעלים:</label>
                              <input type="tel" class="form-control" name="owner_number">
                            </div>
                            <div class="form-group">
                              <label for="childen_amount">כמות ילדים:</label>
                              <input type="number" class="form-control" name="childen_amount">
                            </div>
                            <div class="form-group">
                              <label for="toilet_amount">כמות אסלות:</label>
                              <input type="number" class="form-control" name="toilet_amount">
                            </div>
                            <div class="form-group">
                              <label for="building_size">גודל המבנה:</label>
                              <input type="text" class="form-control" name="building_size">
                            </div>
                            <div class="form-group">
                              <label for="certified_gannet">גננת מוסמכת:</label>
                            <input type="text" class="form-control"  name="certified_gannet"> 
                            </div>
                            <div class="form-group">
                              <label for="Nonconforming">היתר שימוש חורג:</label>
                              <input type="text" class="form-control" name="Nonconforming" > 
                            </div>
                            <div class="form-group">
                              <label for="last_meet">פגישה :</label>
                              <input type="date" class="form-control" name="last_meet">
                            </div>

                            <div class="form-group">
                              <label for="notes">הערות:</label>
                              <textarea  type="textarea" class="form-control" name="notes"  rows="5" id="comment"></textarea>
                            </div><br><br>
                            <input type="submit" name="submit" style="display: block; width: 100%;" class="btn btn-primary" />

                      </form>                    
                  </div>