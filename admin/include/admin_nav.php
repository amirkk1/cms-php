 <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="admin_index.php">אזור מנהל</a>
            </div>
            <!-- /.navbar-header -->

            
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" style="width:200px !important" placeholder="">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li class="active">
                            <a data-toggle="tab" href="#menu1" ><i class="fa fa-fw fa-dashboard"></i> ראשי </a>
                        </li>
                       <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#post"><i class="fa fa-fw fa-arrows-v"></i> ניהול גנים <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="post" class="collapse">
                                <li>
                                <a data-toggle="tab" href="#menu2"><i class="fa fa-edit fa-fw"></i> הוסף גן  </a>
                                 </li>
                                <li>
                                <a data-toggle="tab" href="#menu3"><i class="fa  fa-trash fa-fw"></i> הסר גן </a>
                                </li>
                        </ul>
                    </li>
                        <li>
                                <a data-toggle="tab" href="#menu4"><i class="fa  fa-table fa-fw"></i> (בקרוב) הוסף פגישה </a>
                                </li>
                        <li>
                            <a href="login_area/log_out.php"><i class="fa fa-edit fa-fw"></i> התנתק </a>
                        </li>
                       

                           
                
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>